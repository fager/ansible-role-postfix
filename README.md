fager.postfix
=============

A basic Role to manage Postfix configurations.

Requirements
------------

postfix must be installed on the managed system.

Role Variables
--------------

no

Dependencies
------------

no

Example Playbook
----------------


    - hosts: servers
      roles:
         - { role: fager.postfix, smtp_tls_loglevel: 1 }

License
-------

BSD

Author Information
------------------


